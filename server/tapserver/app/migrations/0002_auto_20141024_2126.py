# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='DummyUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_id', models.CharField(max_length=255)),
                ('polls_sent', models.ManyToManyField(to='app.Poll')),
            ],
            options={
                'verbose_name': 'User',
                'verbose_name_plural': 'Users',
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='option',
            options={'verbose_name': 'Option', 'verbose_name_plural': 'Options'},
        ),
        migrations.AlterModelOptions(
            name='poll',
            options={'verbose_name': 'Poll', 'verbose_name_plural': 'Polls'},
        ),
        migrations.AlterField(
            model_name='option',
            name='logo',
            field=models.ImageField(upload_to=b'media/images/'),
            preserve_default=True,
        ),
    ]
