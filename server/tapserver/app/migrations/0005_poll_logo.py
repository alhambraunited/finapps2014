# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_vote'),
    ]

    operations = [
        migrations.AddField(
            model_name='poll',
            name='logo',
            field=models.ImageField(null=True, upload_to=b'media/images/'),
            preserve_default=True,
        ),
    ]
