# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_auto_20141024_2139'),
    ]

    operations = [
        migrations.CreateModel(
            name='Vote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('option', models.ForeignKey(to='app.Option')),
                ('poll', models.ForeignKey(to='app.Poll')),
                ('user', models.ForeignKey(to='app.DummyUser')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
