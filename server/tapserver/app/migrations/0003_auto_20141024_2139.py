# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20141024_2126'),
    ]

    operations = [
        migrations.RenameField(
            model_name='dummyuser',
            old_name='polls_sent',
            new_name='polls',
        ),
    ]
