from django.db import models

class Poll(models.Model):
    description = models.TextField()
    logo = models.ImageField(null=True, upload_to="media/images/")
    end_date = models.DateTimeField()

    def __unicode__(self):
        return self.description

    class Meta:
        verbose_name = 'Poll'
        verbose_name_plural = 'Polls'


class Option(models.Model):
    poll = models.ForeignKey(Poll)
    name = models.CharField(max_length=255)
    description = models.TextField()
    logo = models.ImageField(upload_to="media/images/")
    url = models.URLField()

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = 'Option'
        verbose_name_plural = 'Options'


class DummyUser(models.Model):
    user_id = models.CharField(max_length=255)
    polls = models.ManyToManyField(Poll)

    def __unicode__(self):
        return self.user_id

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'


class Vote(models.Model):
    user = models.ForeignKey(DummyUser)
    poll = models.ForeignKey(Poll)
    option = models.ForeignKey(Option)
