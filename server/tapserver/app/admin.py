from django.contrib import admin
from models import Poll, Option, DummyUser

class OptionInline(admin.TabularInline):
    model = Option

class PollAdmin(admin.ModelAdmin):
    inlines = [
        OptionInline
    ]

#class PollInline(admin.TabularInline):
#    model = Poll

#class DummyUserAdmin(admin.ModelAdmin):
#    inlines = [
#        PollInline
#    ]

# Register your models here.
admin.site.register(Poll, PollAdmin)
admin.site.register(DummyUser)
