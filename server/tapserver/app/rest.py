from django.core.files.base import ContentFile
from django.views.decorators.csrf import csrf_exempt
from rest_framework import serializers, viewsets, generics
from rest_framework.permissions import AllowAny
from rest_framework.authentication import SessionAuthentication
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from models import Poll, Option, DummyUser, Vote
from datetime import datetime
import base64


class Base64ImageField(serializers.ImageField):

    def to_native(self, obj):
        encoded_string = ""

        with open(obj.path, "rb") as image_file:
            encoded_string = base64.b64encode(image_file.read())

        return encoded_string

# Serializers define the API representation.

class OptionSerializer(serializers.ModelSerializer):
    logo = Base64ImageField()

    class Meta:
        model = Option
        fields = ('id', 'name', 'description', 'logo', 'url')

class PollSerializer(serializers.ModelSerializer):
    logo = Base64ImageField()
    option_set = OptionSerializer(many=True)

    class Meta:
        model = Poll
        fields = ('id', 'description', 'logo', 'end_date', 'option_set')

# ViewSets define the view behavior.
class PollViewSet(viewsets.ModelViewSet):
    queryset = Poll.objects.filter() # TODO: this should be modified
    serializer_class = PollSerializer

class PollList(generics.ListAPIView):
    serializer_class = PollSerializer

    def get_queryset(self):

        username = self.request.QUERY_PARAMS.get('user_id', None)

        polls = Poll.objects.order_by('?')[:1]

        return polls


"""
This is a totally insecure method, this is a prototype :)

"""
class UnsafeSessionAuthentication(SessionAuthentication):

    def authenticate(self, request):
        user = DummyUser.objects.all()[0]

        return (user, None)


class PollResults(APIView):
    permission_classes = (AllowAny,) #maybe not needed in your case
    authentication_classes = (UnsafeSessionAuthentication,)

    def get(self, request, format=None):
        poll_id = request.GET.get('poll')

        poll = Poll.objects.get(id=poll_id)

        votes = Vote.objects.filter(poll=poll)
        poll_options = Option.objects.filter(poll=poll)

        result = {"results": []}
        for option in poll_options:
            result["results"].append({"option": option.id, "votes": Vote.objects.filter(poll=poll, option=option).count()})

        return Response(result)



class PostVote(APIView):
    permission_classes = (AllowAny,) #maybe not needed in your case
    authentication_classes = (UnsafeSessionAuthentication,)

    def post(self, request, format=None):
        user_id = request.DATA.get('user_id')
        poll_id = request.DATA.get('poll')
        option_id = request.DATA.get('option')

        user = DummyUser.objects.get(id=user_id)
        poll = Poll.objects.get(id=poll_id)
        option = Option.objects.get(id=option_id)

        try:
            vote = Vote.objects.get(user=user, poll=poll)
            vote.option = option
        except Vote.DoesNotExist:
            vote = Vote.objects.create(user=user, poll=poll, option=option)

        vote.save()

        poll_options = Option.objects.filter(poll=poll)

        result = {"results": []}
        for option in poll_options:
            result["results"].append({"option": option.id, "votes": Vote.objects.filter(poll=poll, option=option).count()})

        return Response(result)
