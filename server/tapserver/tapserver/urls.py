from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework import routers, serializers, viewsets
from app.models import Poll
from app import rest

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'poll', rest.PollViewSet, 'poll')


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'tapserver.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # ADMIN
    url(r'^admin/', include(admin.site.urls)),

    # API
    url(r'^api/v1/poll/$', rest.PollList.as_view()),
    url(r'^api/v1/poll/vote/$', rest.PostVote.as_view()),
    url(r'^api/v1/poll/results/$', rest.PollResults.as_view()),
    #url(r'^api/v1/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
)
