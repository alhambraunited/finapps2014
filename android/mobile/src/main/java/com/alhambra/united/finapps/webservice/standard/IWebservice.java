package com.alhambra.united.finapps.webservice.standard;

public interface IWebservice {

    public Object callWebservice(Object... params);

    public void notifyChanges(Object result);
}
