package com.alhambra.united.finapps.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;

import com.alhambra.united.finapps.webservice.WebserviceConnector;
import com.alhambra.united.finapps.webservice.standard.IWebservice;

import java.util.Timer;
import java.util.TimerTask;
import com.alhambra.united.finapps.webservice.standard.WebserviceAsyncTask;
import com.alhambra.united.findapps.entity.Poll;
import com.alhambra.united.findapps.googleApiCommunication;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

public class NewPollService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    //#region CONSTANTS
    private static final int sGAP_IN_MS= 20000;
    //#endregion

    //#region ATTRIBUTES
    private Timer mTimer;
    //#endregion

    //#region OVERRIDE METHODS
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final Context context = this;
        googleApiCommunication.createConnection(this);
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                               new WebserviceAsyncTask(context, new WebserviceCallback(context)).execute();
                           }
                        },
                        sGAP_IN_MS,
                        sGAP_IN_MS);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy(){
        if (mTimer != null){
            mTimer.cancel();
            mTimer = null;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
    //#endregion

    //#region WEBSERVICE CALLBACK
    private class WebserviceCallback implements IWebservice {

        private Context mContext;

        public WebserviceCallback(Context context){
            this.mContext = context;
        }

        @Override
        public Object callWebservice(Object... params) {
            return WebserviceConnector.getPool();
        }

        @Override
        public void notifyChanges(Object result) {
            if(result != null && result instanceof Poll){
                googleApiCommunication.sendPoolNotificationMessage((Poll) result);
            }
        }
    }
    //#endregion
}
