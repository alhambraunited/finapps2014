package com.alhambra.united.finapps.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import com.alhambra.united.finapps.storage.DynamicData;
import com.alhambra.united.finapps.webservice.WebserviceConnector;
import com.alhambra.united.finapps.webservice.standard.IWebservice;
import com.alhambra.united.finapps.webservice.standard.WebserviceAsyncTask;
import com.alhambra.united.findapps.entity.Poll;
import com.alhambra.united.findapps.entity.PollStatistic;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class FinalStatisticService extends Service{

    //#region CONSTANTS
    private static final int sGAP_IN_MS= 600000;
    //#endregion

    //#region ATTRIBUTES
    private Timer mTimer;
    //#endregion

    //#region OVERRIDE METHODS
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final Context context = this;
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                               new WebserviceAsyncTask(context, new WebserviceCallback(context)).execute();
                           }
                        },
                        sGAP_IN_MS,
                        sGAP_IN_MS);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy(){
        if (mTimer != null){
            mTimer.cancel();
            mTimer = null;
        }
    }
    //#endregion

    //#region WEBSERVICE CALLBACK
    private class WebserviceCallback implements IWebservice {

        private Context mContext;
        private Poll mPollToRemove;

        WebserviceCallback(Context context){
            this.mContext = context;
            this.mPollToRemove = null;
        }

        @Override
        public Object callWebservice(Object... params) {
            List<Poll> polls = DynamicData.getPolls(mContext);
            if(polls != null && !polls.isEmpty()) {
                try{
                    Date endDate = WebserviceConnector.DATE_FORMATTER.parse(polls.get(0).endDate);
                    if(endDate != null && Calendar.getInstance().getTime().after(endDate)) {
                        mPollToRemove = polls.get(0);
                        return WebserviceConnector.getResults(mPollToRemove.id);
                    }
                }catch(ParseException e){
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        public void notifyChanges(Object result) {
            if(mPollToRemove != null && result != null && result instanceof PollStatistic){
                PollStatistic poll = (PollStatistic) result;
                DynamicData.removePoll(mContext, mPollToRemove.id);
                //TODO Show final statistics!
            }
        }
    }
    //#endregion
}