package com.alhambra.united.finapps;

import android.app.Application;
import android.content.Intent;

import com.alhambra.united.finapps.service.NewPollService;

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        startService(new Intent(this, NewPollService.class));
    }
}
