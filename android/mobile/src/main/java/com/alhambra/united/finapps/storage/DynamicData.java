package com.alhambra.united.finapps.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.alhambra.united.findapps.entity.Option;
import com.alhambra.united.findapps.entity.Poll;

import java.util.ArrayList;
import java.util.List;

public class DynamicData {

    //#region CONSTANTS
    private final static String sDYNAMIC_DATA = "Dynamic Data";

    private final static String sPOLL_ID = "Poll Id";
    private final static String sLOGO = "Logo";
    private final static String sDESCRIPTION = "Description";
    private final static String sEND_DATE = "End Date";
    private final static String sOPTION_ID = "Option ID";
    private final static String sOPTION_NAME = "Option Name";
    private final static String sOPTION_DESCRIPTION = "Option Description";
    private final static String sOPTION_LOGO = "Option Logo";
    private final static String sOPTION_URL = "Option URL";
    //#endregion

    //#region PUBLIC METHODS
    public static void addPoll(Context context, Poll poll){
        if(poll != null){
            SharedPreferences preferences = context.getSharedPreferences(sDYNAMIC_DATA, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();

            editor.putInt(sPOLL_ID + getNextFreePollIndex(preferences), poll.id);
            editor.putString(poll.id + sLOGO, poll.logo != null ? poll.logo : "");
            editor.putString(poll.id + sDESCRIPTION, poll.description != null ? poll.description : "");
            editor.putString(poll.id + sEND_DATE, poll.endDate != null ? poll.endDate : "");
            if(poll.optionSet != null) {
                int numberOfOptions = poll.optionSet.size();
                for (int i = 0; i < numberOfOptions; i++) {
                    addOption(editor, poll.id, i, poll.optionSet.get(i));
                }
            }
            editor.commit();
        }
    }

    public static void removePoll(Context context, int pollID){
        SharedPreferences preferences = context.getSharedPreferences(sDYNAMIC_DATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        int nextFreePollIndex = getNextFreePollIndex(preferences);
        int index = -1;
        for(int i = 0; i < nextFreePollIndex; i++){
            if(preferences.getInt(sPOLL_ID + i, -1) == pollID){
                index = i;
                break;
            }
        }
        if(index > 0)
            removePoll(preferences, pollID, index);

        //Now we will move latest added poll to the possible generated gap
        if(index > 0 && index < nextFreePollIndex - 1){
            Poll latestPoll = getPoll(preferences, nextFreePollIndex - 1);
            removePoll(preferences, pollID, nextFreePollIndex - 1);
            addPoll(context, latestPoll);
        }
    }

    public static List<Poll> getPolls(Context context){
        SharedPreferences preferences = context.getSharedPreferences(sDYNAMIC_DATA, Context.MODE_PRIVATE);
        List<Poll> polls = new ArrayList<Poll>();
        int i = 0;
        while(preferences.contains(sPOLL_ID + i)){
            Poll poll = getPoll(preferences, i);
            if(poll != null)
                polls.add(poll);
        }
        return polls;
    }
    //#endregion

    //#region PRIVATE METHODS
    private static void addOption(SharedPreferences.Editor editor, int pollID, int index, Option option){
        if(option != null){
            editor.putInt(pollID + sOPTION_ID + index, option.id);
            editor.putString(pollID + sOPTION_NAME + index, option.name != null ? option.name : "");
            editor.putString(pollID + sOPTION_DESCRIPTION + index, option.description != null ? option.description : "");
            editor.putString(pollID + sOPTION_LOGO + index, option.logo != null ? option.logo : "");
            editor.putString(pollID + sOPTION_URL + index, option.url != null ? option.url : "");
        }
    }

    private static void removeOption(SharedPreferences.Editor editor, int pollID, int index){
        editor.remove(pollID + sOPTION_ID + index);
        editor.remove(pollID + sOPTION_NAME + index);
        editor.remove(pollID + sOPTION_DESCRIPTION + index);
        editor.remove(pollID + sOPTION_LOGO + index);
        editor.remove(pollID + sOPTION_URL + index);
    }

    private static void removePoll(SharedPreferences preferences, int pollID, int index){
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(sPOLL_ID + index);
        editor.remove(pollID + sLOGO);
        editor.remove(pollID + sDESCRIPTION);
        editor.remove(pollID + sEND_DATE);
        int i = 0;
        while(preferences.contains(pollID + sOPTION_ID + i)){
            removeOption(editor, pollID, i);
            i++;
        }
        editor.commit();
    }

    private static Poll getPoll(SharedPreferences preferences, int index){
        int pollID = preferences.getInt(sPOLL_ID + index, -1);
        if(pollID > 0){
            String logo = preferences.getString(pollID + sLOGO, "");
            String description = preferences.getString(pollID + sDESCRIPTION, "");
            String endDate = preferences.getString(pollID + sEND_DATE, "");
            List<Option> optionSet = new ArrayList<Option>();
            int i = 0;
            while(preferences.contains(pollID + sOPTION_ID + i)){
                optionSet.add(getOption(preferences, pollID, i));
            }
            return new Poll(pollID, logo, description, endDate, optionSet);
        }
        return null;
    }

    private static Option getOption(SharedPreferences preferences, int pollID, int index){
        return new Option(  preferences.getInt(pollID + sOPTION_ID + index, -1),
                preferences.getString(pollID + sOPTION_NAME + index, ""),
                preferences.getString(pollID + sOPTION_DESCRIPTION + index, ""),
                preferences.getString(pollID + sOPTION_LOGO + index, ""),
                preferences.getString(pollID + sOPTION_URL + index, ""));
    }

    private static int getNextFreePollIndex(SharedPreferences preferences){
        int i = 0;
        while(preferences.contains(sPOLL_ID + i)){
            i++;
        }
        return i;
    }
    //#endregion
}