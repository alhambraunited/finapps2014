package com.alhambra.united.finapps.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ServicesStarter extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, NewPollService.class));
        context.startService(new Intent(context, FinalStatisticService.class));
    }
}