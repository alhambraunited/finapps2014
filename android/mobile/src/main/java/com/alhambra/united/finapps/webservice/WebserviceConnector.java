package com.alhambra.united.finapps.webservice;

import com.alhambra.united.findapps.entity.Poll;
import com.alhambra.united.findapps.entity.PollStatistic;
import com.alhambra.united.findapps.entity.Vote;
import com.alhambra.united.finapps.webservice.standard.JSONConnector;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.text.SimpleDateFormat;

public class WebserviceConnector extends JSONConnector {

    //#region CONSTANTS
    private final static String sWEBSERVICE_URL_BASE = "http://finappsapi.xterminatestudio.com/api/v1/poll/";
    private final static String sVOTE = "vote/";
    private final static String sRESULTS = "results/";
    private final static String sPOLL_PARAMETER = "?poll=";

    public static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    private static Gson sGSON_PARSER = new GsonBuilder().create();
    //#endregion

    //#region PUBLIC METHODS
    public static Poll getPool(){
        JSONObject response = sendHttpGet(sWEBSERVICE_URL_BASE);
        try {
            return sGSON_PARSER.fromJson(response.toString(), Poll.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static PollStatistic vote(Vote vote){
        try {
            JSONObject jsonToSend = new JSONObject(sGSON_PARSER.toJson(vote));
            JSONObject response = sendHttpPost(sWEBSERVICE_URL_BASE + sVOTE, jsonToSend);
            return sGSON_PARSER.fromJson(response.toString(), PollStatistic.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static PollStatistic getResults(int pollID){
        try {
            JSONObject response = sendHttpGet(sWEBSERVICE_URL_BASE + sRESULTS
                    + sPOLL_PARAMETER + pollID);
            return sGSON_PARSER.fromJson(response.toString(), PollStatistic.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    //#endregion
}