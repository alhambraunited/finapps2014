package com.alhambra.united.finapps.webservice.standard;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.alhambra.united.finapps.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class JSONConnector {

    //#region CONSTANTS
    private final static String sKEY_NETWORK_ERROR = "Network Error";
    //#endregion

    //#region PUBLIC METHODS
    public static boolean isInternetAvailable(Context context){
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static JSONObject createErrorJSON(Context context){
        JSONObject object = new JSONObject();
        try {
            object.put(sKEY_NETWORK_ERROR, context.getString(R.string.network_error));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }
    //#endregion

    //#region PROTECTED GENERAL METHODS
    protected static JSONObject sendHttpGet(String url) {
        HttpGet getRequest = new HttpGet(url);
        return sendJSONRequest(getRequest);
    }

    protected static JSONArray sendHttpGetArray(String url) {
        HttpGet getRequest = new HttpGet(url);
        return sendJSONArrayRequest(getRequest);
    }

    protected static JSONObject sendHttpPost(String url, JSONObject jsonObjectToSend) {
        try {
            HttpPost postRequest = new HttpPost(url);
            StringEntity se = new StringEntity(jsonObjectToSend.toString());
            postRequest.setEntity(se);
            return sendJSONRequest(postRequest);
        }catch(UnsupportedEncodingException e){
            e.printStackTrace();
        }
        return null;
    }

    protected static JSONObject sendHttpPut(String url, JSONObject jsonObjectToSend) {
        try {
            HttpPut putRequest = new HttpPut(url);
            StringEntity se = new StringEntity(jsonObjectToSend.toString());
            putRequest.setEntity(se);
            return sendJSONRequest(putRequest);
        }catch(UnsupportedEncodingException e){
            e.printStackTrace();
        }
        return null;
    }
    //#endregion

    //#region PRIVATE METHODS
    private static JSONObject sendJSONRequest(HttpRequestBase request){
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();

            request.setHeader("Accept", "application/json");
            request.setHeader("Content-Type", "application/json");

            HttpResponse response = httpClient.execute(request);

            return getJSONResponse(response);
        }catch(IOException e){
            e.printStackTrace();
        }
        return null;
    }

    private static JSONArray sendJSONArrayRequest(HttpRequestBase request){
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();

            request.setHeader("Accept", "application/json");
            request.setHeader("Content-Type", "application/json");

            HttpResponse response = httpClient.execute(request);

            return getJSONArrayResponse(response);
        }catch(IOException e){
            e.printStackTrace();
        }
        return null;
    }

    private static JSONObject getJSONResponse(HttpResponse response){
        try {
            String resultString = getStringResult(response);
            if(resultString != null)
                return new JSONObject(resultString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static JSONArray getJSONArrayResponse(HttpResponse response) {
        try {
            String resultString = getStringResult(response);
            if(resultString != null)
                return new JSONArray(resultString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getStringResult(HttpResponse response){
        try {
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream inStream = entity.getContent();

                String resultString = convertStreamToString(inStream);
                inStream.close();
                if (resultString.startsWith("["))
                    resultString = resultString.substring(1, resultString.length() - 1);
                return resultString;
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        return null;
    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
    //#endregion
}