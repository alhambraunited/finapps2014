package com.alhambra.united.finapps.webservice.standard;

import android.content.Context;
import android.os.AsyncTask;

public class WebserviceAsyncTask extends AsyncTask<Object, Void, Object> {

    //#region ATTRIBUTES
    private Context mContext;
    private IWebservice mCallback;
    //#endregion

    //#region CONSTRUCTORS
    public WebserviceAsyncTask(Context context, IWebservice callback) {
        mContext = context;
        mCallback = callback;
    }
    //#endregion

    //#region OVERRIDE METHODS
    @Override
    protected Object doInBackground(Object... params) {
        if(JSONConnector.isInternetAvailable(mContext))
             return mCallback.callWebservice(params);
        else
            return JSONConnector.createErrorJSON(mContext);
    }

    @Override
    protected void onPostExecute(Object result) {
        mCallback.notifyChanges(result);
    }
    //#endregion
}

