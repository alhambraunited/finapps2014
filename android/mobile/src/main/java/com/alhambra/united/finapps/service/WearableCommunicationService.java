package com.alhambra.united.finapps.service;

import android.util.Log;

import com.alhambra.united.finapps.webservice.WebserviceConnector;
import com.alhambra.united.finapps.webservice.standard.IWebservice;
import com.alhambra.united.finapps.webservice.standard.WebserviceAsyncTask;
import com.alhambra.united.findapps.Constants;
import com.alhambra.united.findapps.entity.PollStatistic;
import com.alhambra.united.findapps.entity.Vote;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.nio.ByteBuffer;

public class WearableCommunicationService extends WearableListenerService{

    private static final String TAG = "WearableCommunicationService";

    private GoogleApiClient mGoogleApiClient;

    private void createConnection(){
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();

        mGoogleApiClient.connect();
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        Log.d(TAG, "On Data Changed ");
        for (DataEvent dataEvent : dataEvents) {
            //TODO Remove useless code
             if (dataEvent.getType() == DataEvent.TYPE_CHANGED) {
                 String path = dataEvent.getDataItem().getUri().getPath();
                 Log.d(TAG, " path received " + path);
                 if (path.compareTo(Constants.PATH_ACTION_VOTE) == 0){
                     DataMapItem dataMapItem = DataMapItem.fromDataItem(dataEvent.getDataItem());
                     int pollId = dataMapItem.getDataMap().getInt(Constants.KEY_POLL_ID);
                     int optionId = dataMapItem.getDataMap().getInt(Constants.KEY_OPTION_ID);
                     Vote vote = new Vote(pollId, optionId);
                     new WebserviceAsyncTask(this, new WebserviceCallback()).execute(vote);
                 }
             }
        }
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.d(TAG, "MessageEvent received: " + messageEvent.getPath());
//        if (messageEvent.getPath().compareTo(Constants.PATH_ACTION_VOTE) == 0){
//            byte[] bytes = messageEvent.getData();
//            int poll = ByteBuffer.wrap(bytes).getInt();
//            int option = ByteBuffer.wrap(bytes).getInt();
//            Vote vote = new Vote(poll, option);
//            new WebserviceAsyncTask(this, new WebserviceCallback()).execute(vote);
//        }
    }

    //#region WEBSERVICE CALLBACK
    private class WebserviceCallback implements IWebservice {

        @Override
        public Object callWebservice(Object... params) {
            return WebserviceConnector.vote((Vote) params[0]);
        }

        @Override
        public void notifyChanges(Object result) {
            if(result != null && result instanceof PollStatistic){
                Log.d(TAG, "Vote sent");
                PollStatistic pollStatistic = (PollStatistic) result;
                createConnection();
                if (mGoogleApiClient.isConnected()) {
                    //TODO Send pollStatistic to wearable

//                    PutDataMapRequest dataMapRequest = PutDataMapRequest.create(Constants.PATH_SEND_FAVOURITES_MESSAGE);
//                    dataMapRequest.getDataMap().putStringArray(Constants.SEND_FAVOURITES_LIST, firstLines);
//                    dataMapRequest.getDataMap().putLong("time", new Date().getTime());
//                    PutDataRequest putDataRequest = dataMapRequest.asPutDataRequest();
//                    Wearable.DataApi.putDataItem(mGoogleApiClient, putDataRequest);
                }
                else {
                    Log.e(TAG, "No connection to wearable available!");
                }
            }
        }
    }
    //#endregion
}
