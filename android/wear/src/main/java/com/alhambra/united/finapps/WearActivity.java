package com.alhambra.united.finapps;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.wearable.view.GridViewPager;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowInsets;

import com.alhambra.united.findapps.Constants;
import com.alhambra.united.findapps.entity.Option;
import com.alhambra.united.findapps.entity.Poll;
import com.alhambra.united.findapps.googleApiCommunication;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.Calendar;

public class WearActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private Poll mPoll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getIntent().getAction().compareTo(Constants.ACTION_SHOW_OPTION_GRID) == 0) {
            setContentView(R.layout.activity_wear);
            googleApiCommunication.createConnection(this);
            Log.d("check", "getIntent");
            Bundle b = getIntent().getExtras();
            if(b != null) {
                mPoll = (Poll) b.getSerializable(Constants.EXTRA_POLL_DATA);
                Log.d("check", "set poll data");
            }
            if (mPoll != null)
                manageGridview();
        } else {
            setContentView(R.layout.activity);
            final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
            stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
                @Override
                public void onLayoutInflated(WatchViewStub stub) {
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("check", "onResume");

    }

    private void manageGridview(){
        final Resources res = getResources();
        final GridViewPager pager = (GridViewPager) findViewById(R.id.pager);
        pager.setOnApplyWindowInsetsListener(new View.OnApplyWindowInsetsListener() {
            @Override
            public WindowInsets onApplyWindowInsets(View v, WindowInsets insets) {
                // Adjust page margins:
                //   A little extra horizontal spacing between pages looks a bit
                //   less crowded on a round display.
                final boolean round = insets.isRound();
                int rowMargin = res.getDimensionPixelOffset(R.dimen.page_row_margin);
                int colMargin = res.getDimensionPixelOffset(round ?
                        R.dimen.page_column_margin_round : R.dimen.page_column_margin);
                pager.setPageMargins(rowMargin, colMargin);
                return insets;
            }
        });
        pager.setAdapter(new SampleGridPagerAdapter(this, getFragmentManager(), mPoll));
        pager.setOnTouchListener(new View.OnTouchListener() {
            private static final int MAX_CLICK_DURATION = 50;
            private long startClickTime;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        startClickTime = Calendar.getInstance().getTimeInMillis();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                        if (clickDuration < MAX_CLICK_DURATION) {
                            if (mPoll != null && mPoll.optionSet != null && pager != null && pager.getCurrentItem() != null && mPoll.optionSet.get(pager.getCurrentItem().y) != null) {
                                Option option = (Option) mPoll.optionSet.get(pager.getCurrentItem().y);
                                sendVote(mPoll.id, option.id, option.name);
                            }
                        }
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    //#region PRIVATE METHODS
    private void sendVote(int pollId, int optionId, String optionName) {
        Intent intent = new Intent(Constants.ACTION_DELAYED_CONFIRMATION, null, this, DelayedConfirmationActivity.class);
        intent.putExtra(Constants.EXTRA_POLL_ID, pollId);
        intent.putExtra(Constants.EXTRA_OPTION_ID, optionId);
        intent.putExtra(Constants.EXTRA_OPTION_NAME, optionName);
        startActivity(intent);
        finish();
    }
    //#endregion
}
