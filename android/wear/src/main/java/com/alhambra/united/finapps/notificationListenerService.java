package com.alhambra.united.finapps;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.alhambra.united.findapps.Constants;
import com.alhambra.united.findapps.entity.Option;
import com.alhambra.united.findapps.entity.Poll;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.data.FreezableUtils;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by mara on 24/10/14.
 */
public class notificationListenerService extends WearableListenerService {

    private static final String sTAG = "notificationListenerService";

    private GoogleApiClient mGoogleApiClient;
    private Asset mPollAsset;
    private List<Asset> mOptionsAsset;

    @Override
    public void onCreate() {
        super.onCreate();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();

        mGoogleApiClient.connect();
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        Log.e(sTAG, "OnDataChanged true????");
        final List<DataEvent> events = FreezableUtils.freezeIterable(dataEvents);
        dataEvents.close();

        if(!mGoogleApiClient.isConnected()){
            ConnectionResult connectionResult = mGoogleApiClient.blockingConnect(5, TimeUnit.SECONDS);
            Log.e(sTAG, "Service failed to connect to GoogleApiClient");
            if (!connectionResult.isSuccess()) {
                Log.e(sTAG, "Service failed to connect to GoogleApiClient");
                return;
            }
        }

        for (DataEvent event : events) {
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                String path = event.getDataItem().getUri().getPath();
                if (path.equals(Constants.PATH_POOL_NOTIFICATION)){
                    DataMapItem dataMapItem = DataMapItem.fromDataItem(event.getDataItem());

                    int pollId = dataMapItem.getDataMap().getInt(Constants.KEY_POLL_ID);
                    String pollDescription = dataMapItem.getDataMap().getString(Constants.KEY_POLL_TITLE);
                    mPollAsset = dataMapItem.getDataMap().getAsset(Constants.KEY_POLL_IMAGE);
                    Poll poll = new Poll(pollId, pollDescription);

                    mOptionsAsset = new ArrayList<Asset>();
                    int optionSize = dataMapItem.getDataMap().getInt(Constants.KEY_OPTIONS_LENGTH);
                    for (int i = 0; i < optionSize; i++){
                        int optionId = dataMapItem.getDataMap().getInt(Constants.KEY_OPTION_ID + i);
                        String optionName = dataMapItem.getDataMap().getString(Constants.KEY_OPTION_NAME + i);
                        String optionDescription = dataMapItem.getDataMap().getString(Constants.KEY_OPTION_DESCRIPTION + i);
                        mOptionsAsset.add(dataMapItem.getDataMap().getAsset(Constants.KEY_OPTION_IMAGE + i));
                        String optionUrl = dataMapItem.getDataMap().getString(Constants.KEY_OPTION_URL + i);
                        Option option = new Option(optionId, optionName, optionDescription, optionUrl);
                        poll.optionSet.add(option);
                    }

                    createPoolNotification(poll);

                } else {
                    Log.d(sTAG, "Unrecognized path: " + path);
                }
            }
        }
    }


    private Notification.Action getPollNotificationAction(Poll poll) {
        Intent notificationIntent = new Intent(this, WearActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.EXTRA_POLL_DATA, poll);
        //TODO try to send mOptionsAsset
        notificationIntent.putExtras(bundle);
        notificationIntent.setAction(Constants.ACTION_SHOW_OPTION_GRID);

        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                0,
                notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        return new Notification.Action.Builder(R.drawable.checkgreen, null, pendingIntent).build();
    }


    private void createPoolNotification(Poll poll) {
        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.au_v3)
                .setContentTitle("Obra social")
                .setContentText(poll.description)
                .setOnlyAlertOnce(true)
                .extend(new Notification.WearableExtender()
                        .addAction(getPollNotificationAction(poll))
                        .setContentAction(0)
                        .setBackground(loadBitmapFromAsset(mPollAsset)));

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(Constants.POOL_NOTIFICATION, notificationBuilder.build());
    }

    private Bitmap loadBitmapFromAsset(Asset asset) {
        if (asset == null) {
            throw new IllegalArgumentException("Asset must be non-null");
        }
        ConnectionResult result = mGoogleApiClient.blockingConnect(3000, TimeUnit.MILLISECONDS);
        if (!result.isSuccess()) {
            Log.d(sTAG, "return null");
            return null;
        }
        // convert asset into a file descriptor and block until it's ready
        InputStream assetInputStream = Wearable.DataApi.getFdForAsset(mGoogleApiClient, asset).await().getInputStream();
        mGoogleApiClient.disconnect();

        if (assetInputStream == null) {
            Log.w(sTAG, "Requested an unknown Asset.");
            return null;
        }
        // decode the stream into a bitmap
        return BitmapFactory.decodeStream(assetInputStream);
    }

}
