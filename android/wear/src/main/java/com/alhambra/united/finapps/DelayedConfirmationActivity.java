package com.alhambra.united.finapps;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.activity.ConfirmationActivity;
import android.support.wearable.view.DelayedConfirmationView;
import android.view.View;
import android.widget.TextView;

import com.alhambra.united.findapps.Constants;
import com.alhambra.united.findapps.googleApiCommunication;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.List;


public class DelayedConfirmationActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, DelayedConfirmationView.DelayedConfirmationListener {

    private static final String sTAG = "DelayedConfirmation";
    private static final int NUM_SECONDS = 5;

    private DelayedConfirmationView delayedConfirmationView;
    private int mPollId;
    private int mOptionId;
    private String mOptionName;

    private boolean mIsTimeSelected;

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        android.util.Log.i(sTAG, "onCreate");
        setContentView(R.layout.delayed_confirmation);
        delayedConfirmationView = (DelayedConfirmationView) findViewById(R.id.delayed_confirmation);
        delayedConfirmationView.setTotalTimeMs(NUM_SECONDS * 1000);
        mIsTimeSelected = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        android.util.Log.i(sTAG, "onResume");

        if(getIntent().getAction().compareTo(Constants.ACTION_DELAYED_CONFIRMATION) == 0){
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                mPollId = extras.getInt(Constants.EXTRA_POLL_ID);
                mOptionId = extras.getInt(Constants.EXTRA_OPTION_ID);
                mOptionName = extras.getString(Constants.EXTRA_OPTION_NAME);

            }
        } else {
            mPollId = 0;
        }

        TextView infoMessage = (TextView) findViewById(R.id.info_message);
        infoMessage.setText("La causa " + mOptionName + " va a ser votada");

        delayedConfirmationView.start();
        delayedConfirmationView.setListener(this);
    }

    @Override
    public void onTimerSelected(View v) {
        android.util.Log.i(sTAG, "onTimerSelected");
        mIsTimeSelected = true;
        finish();
    }

    @Override
    public void onTimerFinished(View v) {
        android.util.Log.i(sTAG, "onTimerFinished " + mIsTimeSelected);
        if (!mIsTimeSelected) {
            googleApiCommunication.sendVoteSelectedMessage(mPollId, mOptionId);
            showConfirmationActivity();

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(Constants.POOL_NOTIFICATION);
        }
        finish();
    }

    private void showConfirmationActivity(){
        Intent intent = new Intent(this, ConfirmationActivity.class);
        intent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE, ConfirmationActivity.SUCCESS_ANIMATION);
        startActivity(intent);
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}