/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.alhambra.united.finapps;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.support.wearable.view.CardFragment;
import android.support.wearable.view.FragmentGridPagerAdapter;
import android.support.wearable.view.ImageReference;

import com.alhambra.united.findapps.entity.Option;
import com.alhambra.united.findapps.entity.Poll;

import java.util.ArrayList;


/**
 * Constructs fragments as requested by the GridViewPager. For each row a
 * different background is provided.
 */
public class SampleGridPagerAdapter extends FragmentGridPagerAdapter {

    private final Context mContext;
    private ArrayList<SimpleRow> mPages;

    public SampleGridPagerAdapter(Context ctx, FragmentManager fm, Poll poll) {
        super(fm);
        mContext = ctx;
        if (poll != null)
            initPages(poll);
    }

    static final int[] BG_IMAGES = new int[] {
            R.drawable.debug_background_1,
            R.drawable.debug_background_2,
            R.drawable.debug_background_3,
            R.drawable.debug_background_4,
            R.drawable.debug_background_5
    };

    private void initPages(Poll poll){
        mPages = new ArrayList<SimpleRow>();
        int length = poll.optionSet.size();

        for (int i=0; i<length; i++){
            Option option = poll.optionSet.get(i);
            SimpleRow row = new SimpleRow();
            row.addPages(new SimplePage(option.name, option.description, R.drawable.ic_launcher));
            mPages.add(row);
        }
    }

    /** A simple container for static data in each row */
    public class SimpleRow {

        ArrayList<SimplePage> mPagesRow = new ArrayList<SimplePage>();

        public void addPages(SimplePage page) {
            mPagesRow.add(page);
        }

        public SimplePage getPages(int index) {
            return mPagesRow.get(index);
        }

        public int size(){
            return mPagesRow.size();
        }
    }

    /** A simple container for static data in each page */
    public class SimplePage {

        public String mTitle;
        public String mText;
        public int mIconId;

        public SimplePage(String title, String text, int iconId) {
            this.mTitle = title;
            this.mText = text;
            this.mIconId = iconId;
        }
    }

    @Override
    public Fragment getFragment(int row, int col) {
        SimplePage page = ((SimpleRow)mPages.get(row)).getPages(col);
        CardFragment fragment = CardFragment.create(page.mTitle, page.mText);
        fragment.setExpansionDirection(CardFragment.EXPAND_DOWN);
        fragment.setExpansionEnabled(true);
        fragment.setCardGravity((int) 1.0f);
        fragment.scrollToTop();
        return fragment;
    }

    @Override
    public ImageReference getBackground(int row, int column) {
        return ImageReference.forDrawable(BG_IMAGES[row % BG_IMAGES.length]);
    }

    @Override
    public int getRowCount() {
        return mPages.size();
    }

    @Override
    public int getColumnCount(int rowNum) {
        return mPages.get(rowNum).size();
    }

}