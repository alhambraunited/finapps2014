package com.alhambra.united.findapps.entity;

import android.graphics.Bitmap;

import com.google.android.gms.wearable.Asset;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Option implements Serializable{

    //#region CONSTANTS
    private static final long serialVersionUID = 1L;
    //#endregion

    //#region ATTRIBUTES
    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("description")
    public String description;

    @SerializedName("logo")
    public String logo;

    @SerializedName("url")
    public String url;

    public Bitmap bitmap;
    //#endregion

    //#region CONSTRUCTS
    public Option(int id, String name, String description, String url){
        this.id = id;
        this.name = name;
        this.description = description;
        this.url = url;
    }
    //#endregion

    //#region CONSTRUCTORS
    public Option(int id, String name, String description, String logo, String url){
        this.id = id;
        this.name = name;
        this.description = description;
        this.logo = logo;
        this.url = url;
    }
    //#endregion

}
