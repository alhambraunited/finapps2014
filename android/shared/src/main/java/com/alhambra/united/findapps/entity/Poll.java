package com.alhambra.united.findapps.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Poll implements Serializable{

    //#region CONSTANTS
    private static final long serialVersionUID = 1L;
    //#endregion

    //#region ATTRIBUTES
    @SerializedName("id")
    public int id;

    @SerializedName("logo")
    public String logo;

    @SerializedName("description")
    public String description;

    @SerializedName("end_date")
    public String endDate;

    @SerializedName("option_set")
    public List<Option> optionSet;


    //#endregion

    //#region CONSTRUCTS
    public Poll(){
        this(0, "");

    }

    public Poll(int id, String description){
        this(id, "", description, "", new ArrayList<Option>());

    }

    public Poll(int id, String logo, String description, String endDate, List<Option> optionSet){
        this.id = id;
        this.logo = logo;
        this.description = description;
        this.endDate = endDate;
        this.optionSet = optionSet;
    }
    //#endregion
}
