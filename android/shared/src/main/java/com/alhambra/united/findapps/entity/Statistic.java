package com.alhambra.united.findapps.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Statistic implements Serializable{

    //#region CONSTANTS
    private static final long serialVersionUID = 1L;
    //#endregion

    //#region ATTRIBUTES
    @SerializedName("votes")
    public int votes;

    @SerializedName("option")
    public int option;
    //#endregion

}
