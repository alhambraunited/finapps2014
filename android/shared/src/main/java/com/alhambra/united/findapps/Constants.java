package com.alhambra.united.findapps;

/**
 * Created by mara on 22/10/14.
 */
public class Constants {

    public static final int POOL_NOTIFICATION = 100;

    public static final String PATH_POOL_NOTIFICATION = "/poolnotification";
    public static final String PATH_DISMISS_NOTIFICATION = "/dismissnotification";
    public static final String PATH_ACTION_VOTE = "/actionvote";

    public static final String KEY_POLL_ID = "pollId";
    public static final String KEY_POLL_TITLE = "pollTitle";
    public static final String KEY_POLL_IMAGE = "pollImage";
    public static final String KEY_OPTIONS_LENGTH = "optionsLength";
    public static final String KEY_OPTION_ID = "optionId";
    public static final String KEY_OPTION_NAME = "optionName";
    public static final String KEY_OPTION_DESCRIPTION = "optionDescription";
    public static final String KEY_OPTION_IMAGE = "optionImage";
    public static final String KEY_OPTION_URL = "optionUrl";

    public static final String KEY_TIMESTAMP = "timestamp";
    public static final String ACTION_SHOW_OPTION_GRID = "ActionShowOptionGrid";

    public static final String EXTRA_POLL_DATA = "ExtraPollData";
    public static final String EXTRA_POLL_ID = "ExtraPollId";
    public static final String EXTRA_OPTION_ID = "ExtraOptionId";
    public static final String EXTRA_OPTION_NAME = "ExtraOptionName";

    public static final String ACTION_DELAYED_CONFIRMATION = "ACTION_DELAYED_CONFIRMATION";

}
