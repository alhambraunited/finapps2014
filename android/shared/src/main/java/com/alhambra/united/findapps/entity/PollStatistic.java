package com.alhambra.united.findapps.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PollStatistic implements Serializable{

    //#region CONSTANTS
    private static final long serialVersionUID = 1L;
    //#endregion

    //#region ATTRIBUTES
    @SerializedName("results")
    public List<Statistic> results;
    //#endregion

}
