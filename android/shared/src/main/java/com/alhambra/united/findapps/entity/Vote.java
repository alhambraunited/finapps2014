package com.alhambra.united.findapps.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Vote implements Serializable{

    //#region CONSTANTS
    private static final long serialVersionUID = 1L;
    //#endregion

    //#region ATTRIBUTES
    @SerializedName("user_id")
    public int userId;

    @SerializedName("poll")
    public int poll;

    @SerializedName("option")
    public int option;
    //#endregion

    public Vote(int poll, int option){
        this.userId = 1;
        this.poll = poll;
        this.option = option;
    }

}
