package com.alhambra.united.findapps;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import com.alhambra.united.findapps.Constants;
import com.alhambra.united.findapps.entity.Option;
import com.alhambra.united.findapps.entity.Poll;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import java.io.ByteArrayOutputStream;
import java.util.Date;

/**
 * Created by mara on 25/10/14.
 */
public class googleApiCommunication {

    private static final String sTAG = "MainActivity";

    private static GoogleApiClient mGoogleApiClient;

    public static void createConnection(Context context){
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Wearable.API)
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) context)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) context)
                .build();

        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    public static void disconnect(){
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    public static void sendVoteSelectedMessage(int pollId, int optionId) {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
//            new AsyncTask<Void, Void, Void>() {
//                @Override
//                protected Void doInBackground(Void... params) {
//                    NodeApi.GetConnectedNodesResult nodes =
//                            Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
//                    for (Node node : nodes.getNodes()) {
//                        MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
//                                mGoogleApiClient, node.getId(), Constants.PATH_DISMISS_NOTIFICATION, null).await();
//                        if (!result.getStatus().isSuccess()) {
//                            Log.e(sTAG, "ERROR: failed to send Message: " + result.getStatus());
//                        }
//                    }
//
//                    return null;
//                }
//            }.execute();
            PutDataMapRequest putDataMapRequest = PutDataMapRequest.create(Constants.PATH_ACTION_VOTE);

            putDataMapRequest.getDataMap().putInt(Constants.KEY_POLL_ID, pollId);
            putDataMapRequest.getDataMap().putInt(Constants.KEY_OPTION_ID, optionId);
            putDataMapRequest.getDataMap().putLong(Constants.KEY_TIMESTAMP, new Date().getTime());

            PutDataRequest request = putDataMapRequest.asPutDataRequest();

            Wearable.DataApi.putDataItem(mGoogleApiClient, request)
                    .setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
                        @Override
                        public void onResult(DataApi.DataItemResult dataItemResult) {
                            Log.d(sTAG, "putDataItem status: " + dataItemResult.getStatus().toString());
                        }
                    });


        }
    }

    public static void sendPoolNotificationMessage(Poll poll) {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            PutDataMapRequest putDataMapRequest = PutDataMapRequest.create(Constants.PATH_POOL_NOTIFICATION);

            putDataMapRequest.getDataMap().putInt(Constants.KEY_POLL_ID, poll.id);
            putDataMapRequest.getDataMap().putString(Constants.KEY_POLL_TITLE, String.format(poll.description));
            byte[] decodedString = Base64.decode(poll.logo, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            Asset asset = createAssetFromBitmap(decodedByte);
            putDataMapRequest.getDataMap().putAsset(Constants.KEY_POLL_IMAGE, asset);
            int optionSize = poll.optionSet.size();
            putDataMapRequest.getDataMap().putInt(Constants.KEY_OPTIONS_LENGTH, optionSize);
            for (int i = 0; i < optionSize; i++) {
                Option option = poll.optionSet.get(i);
                putDataMapRequest.getDataMap().putInt(Constants.KEY_OPTION_ID + i, option.id);
                putDataMapRequest.getDataMap().putString(Constants.KEY_OPTION_NAME + i, option.name);
                putDataMapRequest.getDataMap().putString(Constants.KEY_OPTION_DESCRIPTION + i, option.description);
                decodedString = Base64.decode(option.logo, Base64.DEFAULT);
                decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                asset = createAssetFromBitmap(decodedByte);
                putDataMapRequest.getDataMap().putAsset(Constants.KEY_OPTION_IMAGE + i, asset);
                putDataMapRequest.getDataMap().putString(Constants.KEY_OPTION_URL +i, option.url);
            }
            putDataMapRequest.getDataMap().putLong(Constants.KEY_TIMESTAMP, new Date().getTime());

            PutDataRequest request = putDataMapRequest.asPutDataRequest();

            Wearable.DataApi.putDataItem(mGoogleApiClient, request)
                    .setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
                        @Override
                        public void onResult(DataApi.DataItemResult dataItemResult) {
                            Log.d(sTAG, "putDataItem status: " + dataItemResult.getStatus().toString());
                        }
                    });
        }
    }

    private static Asset createAssetFromBitmap(Bitmap bitmap) {
        final ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteStream);
        return Asset.createFromBytes(byteStream.toByteArray());
    }


}
